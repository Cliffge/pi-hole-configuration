# Pi-hole Configuration

## Project Folders

### Adlists

It contains the backup of the adlists added by internet.

### Backup

It contains the last backup of Pi-hole.

### Lists

It contains whitelist and blacklist files.

### Scripts

It contains scripts to manage lists.