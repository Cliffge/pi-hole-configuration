import csv
import os
import re
import shutil
import sys
import urllib3


with open('../Adlists/url_list.csv', 'r') as urls:

    url_csv =  csv.DictReader(urls)

    for url in url_csv:
        if not os.path.isdir('../Adlists/' + url['group']):
            try:
                os.makedirs('../Adlists/' + url['group'])
            except Exception as e:
                print(e)
                sys.exit(1)

        if 'raw.githubusercontent.com' in url['url']:
            file_name = re.sub(r"^((http|https)://)(\braw\.githubusercontent\.com)/|(\s+|/|<|>|:|\"|\\|\||\?|\*|#|=|@)", r"_", url['url']).replace("_", "", 1)
        elif 's3.amazonaws.com' in url:
            file_name = re.sub(r"^((http|https)://)(\bs3\.amazonaws\.com)/|(\s+|/|<|>|:|\"|\\|\||\?|\*|#|=|@)", r"_", url['url']).replace("_", "", 1)
        else:
            file_name = re.sub(r"^((http|https)://)|(\s+|/|<|>|:|\"|\\|\||\?|\*|#|=|@)", r"_", url['url']).replace("_", "", 1)

        try:
            pool = urllib3.PoolManager()
            response = pool.request('GET', url['url'], preload_content=False)
        except Exception as e:
            self_signed_pool = urllib3.PoolManager(cert_reqs='CERT_NONE')
            response = self_signed_pool.request('GET', url['url'], preload_content=False)

        adlist = open('../Adlists/' + url['group'] + '/' + file_name, 'wb')
        shutil.copyfileobj(response, adlist)
        print(url['url'] + ' downloaded successfully')
